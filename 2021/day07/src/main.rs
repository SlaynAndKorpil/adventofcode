/// Calculate the fuel needed for the crabs to reach a certain target position using the
/// function `fuel_costs` to calculate how much fuel moving a certain distance costs.
fn needed_fuel(target: i64, positions: &[i64], fuel_costs: fn(i64) -> i64) -> i64 {
	positions
		.iter()
		.fold(0, |acc, pos| acc + fuel_costs((target - *pos).abs()))
}

fn part1(mut avg: i64, positions: &[i64]) {
	let mut fuel = needed_fuel(avg, positions, |dist| dist);

	// starting at the needed fuel for the average, go either below or above the average
	// until the needed fuel is at a minimum.
	// Since it should always either increase or decrease, this solution is pretty slow
	// when the searched target position is above the starting value. Instead we should
	// first test if we are above or below the minimum and then only search in that
	// direction.
	loop {
		let new_fuel = needed_fuel(avg - 1, positions, |dist| dist);
		if new_fuel < fuel {
			avg -= 1;
			fuel = new_fuel;
			continue;
		}

		let new_fuel = needed_fuel(avg + 1, positions, |dist| dist);
		if new_fuel < fuel {
			avg += 1;
			fuel = new_fuel;
			continue;
		}

		break;
	}

	println!("Part1:\ntarget: {}\nfuel: {}", avg, fuel);
}

/// Calculate the incrementing fuel costs for part 2.
///
/// ~~Pretty sure it should be possible to calculate this easier/faster.~~
/// Nevermind, apparently the compiler can optimize the loop out. Don't even know what it
/// does but it seems sort of correct.
fn inc_fuel_cost(dist: i64) -> i64 {
	let mut cost = 0;

	for i in 0..dist {
		cost += i + 1;
	}

	cost
}

fn part2(mut avg: i64, positions: &[i64]) {
	let mut fuel = needed_fuel(avg, positions, inc_fuel_cost);

	// starting at the needed fuel for the average, go either below or above the average
	// until the needed fuel is at a minimum.
	loop {
		let new_fuel = needed_fuel(avg - 1, positions, inc_fuel_cost);
		if new_fuel < fuel {
			avg -= 1;
			fuel = new_fuel;
			continue;
		}

		let new_fuel = needed_fuel(avg + 1, positions, inc_fuel_cost);
		if new_fuel < fuel {
			avg += 1;
			fuel = new_fuel;
			continue;
		}

		break;
	}

	println!("\n\nPart2:\ntarget: {}\nfuel: {}", avg, fuel);
}

fn main() -> std::io::Result<()> {
	let positions: Vec<i64> = std::fs::read_to_string("input")?
		.trim()
		.split(',')
		.map(|val| val.parse().unwrap())
		.collect();

	let avg: i64 = positions.iter().sum::<i64>() / positions.len() as i64;

	part1(avg, &positions);
	part2(avg, &positions);

	Ok(())
}
