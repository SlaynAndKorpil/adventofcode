fn main() -> std::io::Result<()> {
	let input = std::fs::read_to_string("input")?;

	part1(&input);
	part2(&input);

	Ok(())
}

fn part1(input: &str) {
	let rounds: Vec<_> = input
		.trim()
		.lines()
		.map(|l| l.parse::<Round>().unwrap())
		.collect();
	let score: Score = rounds.iter().map(|round| round.score()).sum();

	println!("Score part 1: {score}");
}

fn part2(input: &str) {
	let lines = input.trim().lines();

	let score: Score = lines
		.map(|line| {
			// format: {A | B | C} {X | Y | Z}
			// parse the first part into a shape and the second part into a result
			let mut chars = line.chars();
			let opponent: Shape = chars.next().unwrap().try_into().unwrap();
			let result: GameResult = match chars.nth(1).unwrap() {
				'X' => GameResult::Loss,
				'Y' => GameResult::Draw,
				'Z' => GameResult::Win,
				x => panic!("Unknown game result: {x}"),
			};

			let round = Round::from(opponent, result);
			round.score()
		})
		.sum();

	println!("Score part 2: {score}");
}

type Score = i32;

#[derive(Clone, Copy, Debug)]
enum GameResult {
	Win,
	Draw,
	Loss,
}

impl GameResult {
	fn score(&self) -> Score {
		match self {
			GameResult::Win => 6,
			GameResult::Draw => 3,
			GameResult::Loss => 0,
		}
	}
}

#[derive(Clone, Copy, Debug)]
struct Round {
	own: Shape,
	opponent: Shape,
}

impl Round {
	// For part 2
	fn from(opponent: Shape, result: GameResult) -> Round {
		use GameResult::*;
		use Shape::*;

		let own = match (opponent, result) {
			(Rock, Draw) => Rock,
			(Rock, Win) => Paper,
			(Rock, Loss) => Scissors,
			(Paper, Loss) => Rock,
			(Paper, Draw) => Paper,
			(Paper, Win) => Scissors,
			(Scissors, Win) => Rock,
			(Scissors, Loss) => Paper,
			(Scissors, Draw) => Scissors,
		};

		Round { own, opponent }
	}

	/// Determins if you win this round.
	fn result(&self) -> GameResult {
		use GameResult::*;
		use Shape::*;

		match (self.own, self.opponent) {
			(Rock, Rock) => Draw,
			(Rock, Paper) => Loss,
			(Rock, Scissors) => Win,
			(Paper, Rock) => Win,
			(Paper, Paper) => Draw,
			(Paper, Scissors) => Loss,
			(Scissors, Rock) => Loss,
			(Scissors, Paper) => Win,
			(Scissors, Scissors) => Draw,
		}
	}

	/// Fights the battle and returns the score.
	fn score(&self) -> Score {
		self.own.score() + self.result().score()
	}
}

impl std::str::FromStr for Round {
	type Err = ();

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		// Uses the definitions from part 1.
		let mut shapes = s
			.trim()
			.split(' ')
			.flat_map(|s| s.chars())
			.map(|c| c.try_into());
		let opponent = shapes.next().unwrap().map_err(|_| ())?;
		let own = shapes.next().unwrap().map_err(|_| ())?;

		Ok(Round { opponent, own })
	}
}

#[derive(Clone, Copy, Debug)]
enum Shape {
	Rock,
	Paper,
	Scissors,
}

impl Shape {
	fn score(&self) -> Score {
		match self {
			Shape::Rock => 1,
			Shape::Paper => 2,
			Shape::Scissors => 3,
		}
	}
}

impl TryFrom<char> for Shape {
	type Error = ();

	fn try_from(c: char) -> Result<Self, Self::Error> {
		// Uses the definitions from part 1.
		match c {
			'A' | 'X' => Ok(Shape::Rock),
			'B' | 'Y' => Ok(Shape::Paper),
			'C' | 'Z' => Ok(Shape::Scissors),
			_ => Err(()),
		}
	}
}
