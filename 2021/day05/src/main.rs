use std::cmp::Ordering;
use std::collections::btree_map::BTreeMap;
use std::num::ParseIntError;

enum IoOrIntError {
	Io(std::io::Error),
	Int(ParseIntError),
}

impl std::fmt::Display for IoOrIntError {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		f.write_str(&match self {
			IoOrIntError::Io(e) => e.to_string(),
			IoOrIntError::Int(e) => e.to_string(),
		})?;

		Ok(())
	}
}

impl From<ParseIntError> for IoOrIntError {
	fn from(x: ParseIntError) -> IoOrIntError {
		IoOrIntError::Int(x)
	}
}

impl From<std::io::Error> for IoOrIntError {
	fn from(x: std::io::Error) -> IoOrIntError {
		IoOrIntError::Io(x)
	}
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
struct Point {
	x: u32,
	y: u32,
}

impl Point {
	fn new(x: u32, y: u32) -> Point {
		Point { x, y }
	}
}

impl std::str::FromStr for Point {
	type Err = IoOrIntError;

	fn from_str(s: &str) -> Result<Point, IoOrIntError> {
		let parts: Vec<&str> = s.split(',').collect();

		if parts.len() != 2 {
			let err_msg = format!("A line is expected to contain two parts seperated by a \",\".\nInstead got \"{}\"", s);
			return Err(
				std::io::Error::new(std::io::ErrorKind::InvalidData, err_msg).into(),
			);
		}

		Ok(Point::new(parts[0].parse()?, parts[1].parse()?))
	}
}

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
struct Line(Point, Point);

impl Line {
	fn new(p1: Point, p2: Point) -> Line {
		Line(p1, p2)
	}

	fn is_orthogonal(&self) -> bool {
		self.0.x == self.1.x || self.0.y == self.1.y
	}

	/// Returns all points which are covered by this line
	fn points(&self) -> Vec<Point> {
		let mut points = Vec::new();

		let x_sig = match self.0.x.cmp(&self.1.x) {
			Ordering::Less => 1,
			Ordering::Equal => 0,
			Ordering::Greater => -1,
		};
		let y_sig = match self.0.y.cmp(&self.1.y) {
			Ordering::Less => 1,
			Ordering::Equal => 0,
			Ordering::Greater => -1,
		};

		let mut x = self.0.x as i64;
		let mut y = self.0.y as i64;

		points.push(Point::new(x as u32, y as u32));

		while x != self.1.x as i64 || y != self.1.y as i64 {
			x += x_sig;
			y += y_sig;

			points.push(Point::new(x as u32, y as u32));
		}

		points
	}
}

impl std::str::FromStr for Line {
	type Err = IoOrIntError;

	fn from_str(s: &str) -> Result<Line, IoOrIntError> {
		let parts: Vec<&str> = s.split(" -> ").collect();

		if parts.len() != 2 {
			let err_msg = format!("A line is expected to contain two parts seperated by a \" -> \".\nInstead got \"{}\"", s);
			Err(std::io::Error::new(std::io::ErrorKind::InvalidData, err_msg).into())
		} else {
			Ok(Line::new(parts[0].parse()?, parts[1].parse()?))
		}
	}
}

fn part1(input: &str) {
	let points = input
		.lines()
		.map(|line| {
			line.parse::<Line>()
				.unwrap_or_else(|e| panic!("Invalid line \"{}\":\n{}", line, e))
		})
		.filter(|l| l.is_orthogonal()) // the only difference to part2
		.map(|line| line.points())
		.flatten();

	let mut point_count = BTreeMap::new();

	// Count all points of every line
	for point in points {
		*point_count.entry(point).or_insert(0u32) += 1u32;
	}

	// The amount of points which covered by at least 2 lines
	let res = point_count
		.into_iter()
		.filter(|(_, count)| *count > 1)
		.count();

	println!("Part 1: {}", res);
}

fn part2(input: &str) {
	let points = input
		.lines()
		.map(|line| {
			line.parse::<Line>()
				.unwrap_or_else(|e| panic!("Invalid line \"{}\":\n{}", line, e))
		})
		.map(|line| line.points())
		.flatten();

	let mut point_count = BTreeMap::new();

	// Count all points of every line
	for point in points {
		*point_count.entry(point).or_insert(0u32) += 1u32;
	}

	// The amount of points which covered by at least 2 lines
	let res = point_count
		.into_iter()
		.filter(|(_, count)| *count > 1)
		.count();

	println!("Part 2: {}", res);
}

fn main() -> std::io::Result<()> {
	let input = std::fs::read_to_string("input")?;

	part1(&input);
	part2(&input);

	Ok(())
}
