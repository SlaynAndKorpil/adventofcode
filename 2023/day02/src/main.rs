use std::str::FromStr;

struct Game {
	id: u32,
	max_red: u32,
	max_green: u32,
	max_blue: u32,
	hands: Vec<Hand>,
}

impl FromStr for Game {
	type Err = ();

	fn from_str(from: &str) -> Result<Game, Self::Err> {
		if let Some((game, hands)) = from.split_once(':') {
			let Some((_, id)) = game.split_once(' ') else {
				return Err(());
			};

			let id = id.parse::<u32>().unwrap();
			let hands = hands
				.split(';')
				.map(str::trim)
				.map(Hand::from_str)
				.map(Result::unwrap)
				.collect::<Vec<_>>();
			let max_red = hands.iter().map(|hand| hand.red).max().unwrap_or(0);
			let max_green = hands.iter().map(|hand| hand.green).max().unwrap_or(0);
			let max_blue = hands.iter().map(|hand| hand.blue).max().unwrap_or(0);

			Ok(Game {
				id,
				max_red,
				max_green,
				max_blue,
				hands,
			})
		} else {
			Err(())
		}
	}
}

#[derive(Copy, Clone, Eq, PartialEq)]
struct Hand {
	red: u32,
	green: u32,
	blue: u32,
}

impl FromStr for Hand {
	type Err = ();

	fn from_str(from: &str) -> Result<Hand, Self::Err> {
		let mut red = 0;
		let mut green = 0;
		let mut blue = 0;

		for balls in from.split(',') {
			let Some((count, color)) = balls.trim().split_once(' ') else {
				return Err(());
			};

			let count = count.trim().parse::<u32>().unwrap();
			let color = color.parse::<Color>()?;

			match color {
				Color::Red => red += count,
				Color::Green => green += count,
				Color::Blue => blue += count,
			}
		}

		Ok(Hand { red, green, blue })
	}
}

#[derive(Copy, Clone, Eq, PartialEq)]
enum Color {
	Red,
	Green,
	Blue,
}

impl FromStr for Color {
	type Err = ();

	fn from_str(from: &str) -> Result<Color, Self::Err> {
		match from.trim() {
			"red" => Ok(Color::Red),
			"green" => Ok(Color::Green),
			"blue" => Ok(Color::Blue),
			_ => Err(()),
		}
	}
}

fn main() -> std::io::Result<()> {
	let input = std::fs::read_to_string("input")?;
	let sum = input
		.lines()
		.map(Game::from_str)
		.map(Result::unwrap)
		.filter(|game| game.max_red <= 12 && game.max_green <= 13 && game.max_blue <= 14)
		.map(|game| game.id)
		.sum::<u32>();

	println!("Sum: {sum}");
	Ok(())
}
