use std::fs::File;
use std::io::Read;

fn part1(inputs: &str) {
	// Every line contains exactly 12 characters
	let mut bit_counts = [0i32; 12];

	for line in inputs.lines() {
		for (i, c) in line.chars().enumerate() {
			match c {
				'0' => bit_counts[i] -= 1,
				'1' => bit_counts[i] += 1,
				_ => panic!("char {} is neither '0' nor '1'!", c),
			}
		}
	}

	// Each number has 12 bit so the product will always fit in 32 bit
	let mut gamma_rate = 0u32;
	let mut epsilon_rate = 0u32;

	for (i, bit_count) in bit_counts.into_iter().rev().enumerate() {
		if bit_count == 0 {
			panic!(
				"The task does not define what happens when there is no most common bit!"
			)
		}

		gamma_rate |= ((bit_count > 0) as u32) << i;
		epsilon_rate |= ((bit_count < 0) as u32) << i;
	}

	println!("Gamma   rate: {:012b}", gamma_rate);
	println!("Epsilon rate: {:012b}", epsilon_rate);

	// Can never overflow since both rates are only 12 bit but are stored in 32 bit.
	let result: u32 = gamma_rate * epsilon_rate;

	println!("Result: {}", result);
}

fn part2(input: &str) {
	// idx of the bit we are currently testing for
	let mut depth = 0;
	let mut oxygen_filter = String::new();

	let oxygen_generator = loop {
		let mut bit_count = 0i32;

		for val in input.lines().filter(|s| s.starts_with(&oxygen_filter)) {
			bit_count += match val.chars().nth(depth) {
				Some(c) => match c {
					'0' => -1,
					'1' => 1,
					x => panic!("Got character {} which is neither '0' nor '1'!", x),
				},
				None => panic!("Did not find a fitting value for oxygen generator!"),
			};
		}

		if bit_count >= 0 {
			oxygen_filter.push('1');
		} else {
			oxygen_filter.push('0');
		}

		let filtered = input
			.lines()
			.filter(|s| s.starts_with(&oxygen_filter))
			.collect::<Vec<&str>>();
		if filtered.len() == 1 {
			break filtered[0];
		}

		depth += 1;
	};

	let mut depth = 0;
	let mut co2_filter = String::new();
	let co2_scrubber = loop {
		let mut bit_count = 0i32;

		for val in input.lines().filter(|s| s.starts_with(&co2_filter)) {
			bit_count += match val.chars().nth(depth) {
				Some(c) => match c {
					'0' => -1,
					'1' => 1,
					x => panic!("Got character {} which is neither '0' nor '1'!", x),
				},
				None => panic!("Did not find a fitting value for co2 scrubber!"),
			};
		}

		if bit_count >= 0 {
			co2_filter.push('0');
		} else {
			co2_filter.push('1');
		}

		let filtered = input
			.lines()
			.filter(|s| s.starts_with(&co2_filter))
			.collect::<Vec<&str>>();
		if filtered.len() == 1 {
			break filtered[0];
		}

		depth += 1;
	};

	let mut oxy_num = 0u32;
	for (i, c) in oxygen_generator.chars().rev().enumerate() {
		oxy_num |= ((c == '1') as u32) << i;
	}

	let mut co2_num = 0u32;
	for (i, c) in co2_scrubber.chars().rev().enumerate() {
		co2_num |= ((c == '1') as u32) << i;
	}

	println!("ox: {:012b}, co2: {:012b}", oxy_num, co2_num);
	println!("Result: {}", oxy_num * co2_num);
}

fn main() -> std::io::Result<()> {
	let mut input_file = File::open("input")?;
	let mut inputs = String::new();
	input_file.read_to_string(&mut inputs)?;

	part1(&inputs);
	part2(&inputs);

	Ok(())
}
