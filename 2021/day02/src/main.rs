use std::fs::File;
use std::io::Read;

#[derive(Debug, Clone, Copy)]
enum Command {
	Down(u32),
	Up(u32),
	Forward(u32),
}

#[derive(Debug)]
enum ParseCommandError {
	UnknownCommand,
	ParseIntError,
}

impl std::fmt::Display for ParseCommandError {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match &self {
			ParseCommandError::UnknownCommand => write!(f, "UnknownCommand")?,
			ParseCommandError::ParseIntError => write!(f, "ParseIntError")?,
		};

		Ok(())
	}
}

impl std::error::Error for ParseCommandError {}

impl std::str::FromStr for Command {
	type Err = ParseCommandError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let parts: Vec<&str> = s.split(' ').collect();

		if parts.len() != 2 {
			return Err(ParseCommandError::UnknownCommand);
		}

		let amount = parts[1]
			.parse::<u32>()
			.or(Err(ParseCommandError::ParseIntError))?;

		match parts[0] {
			"down" => Ok(Command::Down(amount)),
			"up" => Ok(Command::Up(amount)),
			"forward" => Ok(Command::Forward(amount)),
			_ => Err(ParseCommandError::UnknownCommand),
		}
	}
}

#[derive(Debug)]
struct Position {
	horiz: u32,
	depth: u32,
}

impl Position {
	pub fn new() -> Self {
		Position { horiz: 0, depth: 0 }
	}

	pub fn result(&self) -> u32 {
		// Assuming the result always fits in 32 bit
		self.horiz.checked_mul(self.depth).unwrap()
	}
}

#[derive(Debug)]
struct Submarine {
	pos: Position,
	aim: u32,
}

impl Submarine {
	pub fn new() -> Self {
		Submarine {
			pos: Position::new(),
			aim: 0,
		}
	}
}

fn part1(pos: &mut Position, commands: Vec<Command>) {
	for command in commands {
		match command {
			Command::Up(x) => pos.depth -= x,
			Command::Down(x) => pos.depth += x,
			Command::Forward(x) => pos.horiz += x,
		}
	}
}

fn part2(sub: &mut Submarine, commands: Vec<Command>) {
	for command in commands {
		match command {
			Command::Up(x) => sub.aim -= x,
			Command::Down(x) => sub.aim += x,
			Command::Forward(x) => {
				sub.pos.horiz += x;
				sub.pos.depth += sub.aim * x;
			}
		}
	}
}

fn main() -> std::io::Result<()> {
	let mut input_file = File::open("input")?;
	let mut inputs = String::new();
	input_file.read_to_string(&mut inputs)?;

	let inputs: Vec<Command> = inputs
		.lines()
		.filter_map(|input| match input.parse::<Command>() {
			Ok(com) => Some(com),
			Err(error) => {
				eprintln!("! Could not parse {}: {} !", input, error);
				None
			}
		})
		.collect();

	let mut position = Position::new();
	part1(&mut position, Vec::clone(&inputs));
	println!("Part 1:");
	println!("{:?}", position);
	println!("Result: {}", position.result());

	let mut sub = Submarine::new();
	part2(&mut sub, inputs);
	println!("\nPart 2:");
	println!("{:?}", sub);
	println!("Result: {}", sub.pos.result());

	Ok(())
}
