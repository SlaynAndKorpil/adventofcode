/// The pattern of the seven-segment display.
#[derive(Clone, Copy, Debug)]
struct Digit(u8);

impl std::str::FromStr for Digit {
	type Err = String;

	fn from_str(s: &str) -> Result<Digit, Self::Err> {
		let mut a = false;
		let mut b = false;
		let mut c = false;
		let mut d = false;
		let mut e = false;
		let mut f = false;
		let mut g = false;

		for ch in s.chars() {
			match ch {
				'a' => a = true,
				'b' => b = true,
				'c' => c = true,
				'd' => d = true,
				'e' => e = true,
				'f' => f = true,
				'g' => g = true,
				_ => return Err("Invalid character found!".to_owned()),
			}
		}

	}
}

/// An entry in the input
#[derive(Debug)]
struct Entry {
	sig_patterns: [Digit; 10],
	output_value: [Digit; 4],
}

impl std::str::FromStr for Entry {
	type Err = String;

	fn from_str(s: &str) -> Result<Entry, Self::Err> {
		let mut parts = s.trim().split(" | ");

		let mut sig_patterns = [Digit(0); 10];
		if let Some(parts) = parts.next() {
			let patterns: Vec<&str> = parts.split(' ').collect();

			if patterns.len() != 10 {
				return Err("There have to be 10 Signal patterns!".to_owned());
			}

			for (i, pattern) in patterns.iter().map(|pat| pat.parse()).enumerate() {
				match pattern {
					Ok(pattern) => sig_patterns[i] = pattern,
					Err(e) => return Err(e),
				}
			}
		} else {
			return Err("Missing Signal patterns on the left side!".to_owned());
		};

		let mut output_value = [Digit(0); 4];
		if let Some(parts) = parts.next() {
			let patterns: Vec<&str> = parts.split(' ').collect();

			if patterns.len() != 4 {
				return Err("There have to be 4 output patterns!".to_owned());
			}

			for (i, pattern) in patterns.iter().map(|pat| pat.parse()).enumerate() {
				match pattern {
					Ok(pattern) => output_value[i] = pattern,
					Err(e) => return Err(e),
				}
			}
		} else {
			return Err("Missing output value on the right side!".to_owned());
		};

		Ok(Entry {
			sig_patterns,
			output_value,
		})
	}
}

fn part1(input: &str) {
	let mut counter = 0;

	for line in input.lines() {
		for digit in line.split(" | ").nth(1).unwrap().split(' ') {
			match digit.len() {
				// digits 1, 4, 7, 8
				2 | 3 | 4 | 7 => counter += 1,
				_ => {}
			}
		}
	}

	println!("part1: {}", counter);
}

fn main() -> std::io::Result<()> {
	let input = std::fs::read_to_string("input")?;

	part1(&input);

	let entries: Vec<Entry> = input
		.lines()
		.map(|line| line.parse::<Entry>().unwrap())
		.collect();

	println!("{:#?}", entries);

	Ok(())
}
