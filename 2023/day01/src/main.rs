fn main() -> std::io::Result<()> {
	let calibration = part_two()?;
	println!("Calibration value: {calibration}");

	Ok(())
}

fn part_two() -> std::io::Result<u32> {
	let input = std::fs::read_to_string("input")?;
	Ok(input.lines().map(get_calibration_value_two).sum())
}

fn get_calibration_value_two(line: &str) -> u32 {
	let mut first = 0;
	let mut first_idx = line.len();
	let mut last = 0;
	let mut last_idx = 0;

	for (val, num) in [
		(1, "one"),
		(2, "two"),
		(3, "three"),
		(4, "four"),
		(5, "five"),
		(6, "six"),
		(7, "seven"),
		(8, "eight"),
		(9, "nine"),
		(1, "1"),
		(2, "2"),
		(3, "3"),
		(4, "4"),
		(5, "5"),
		(6, "6"),
		(7, "7"),
		(8, "8"),
		(9, "9"),
	] {
		if let Some(pos) = line.find(num) {
			if pos <= first_idx {
				first_idx = pos;
				first = val;
			}
		}

		if let Some(pos) = line.rfind(num) {
			if pos >= last_idx {
				last_idx = pos;
				last = val;
			}
		}
	}

	first * 10 + last
}

fn part_one() -> std::io::Result<u32> {
	Ok(std::fs::read("input")?
		.split(|&x| x == b'\n')
		.map(get_calibration_value_one)
		.sum::<u32>())
}

fn get_calibration_value_one(line: &[u8]) -> u32 {
	let mut sum = 0;

	for b in line {
		if (0x30..=0x39).contains(b) {
			sum += u32::from(b - 0x30) * 10;
			break;
		}
	}

	for b in line.iter().rev() {
		if (0x30..=0x39).contains(b) {
			sum += u32::from(b - 0x30);
			break;
		}
	}

	sum
}
