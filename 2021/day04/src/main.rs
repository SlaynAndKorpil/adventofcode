use std::fs::File;
use std::io::Read;

#[derive(Debug, Clone)]
struct BingoTable {
	table: [[u32; 5]; 5],
}

impl BingoTable {
	/// The score is the sum of all elements in the table that are not already marked.
	fn score(&self, marked_nums: &[u32]) -> u32 {
		self.table
			.map(|row| row.iter().filter(|x| !marked_nums.contains(x)).sum())
			.iter()
			.sum()
	}

	fn completed(&self, marked_nums: &[u32]) -> bool {
		// check rows
		'row: for row in self.table {
			for val in row {
				if !marked_nums.contains(&val) {
					continue 'row;
				}
			}

			return true;
		}

		// check columns
		'col: for column in 0..5 {
			for row in self.table {
				if !marked_nums.contains(&row[column]) {
					continue 'col;
				}
			}

			return true;
		}

		false
	}
}

impl std::str::FromStr for BingoTable {
	type Err = std::io::Error;

	fn from_str(s: &str) -> Result<BingoTable, std::io::Error> {
		let mut bingo_table = [[0; 5]; 5];

		let lines = s.lines().collect::<Vec<&str>>();
		if lines.len() != 5 {
			return Err(std::io::Error::new(
				std::io::ErrorKind::InvalidData,
				"All tables must have 5 rows!",
			));
		}

		for (i, line) in lines.iter().enumerate() {
			let vals = line
				.split(' ')
				// We have to filter empty strings out since there are some double spaces
				.filter(|&s| !s.is_empty())
				.collect::<Vec<&str>>();

			if vals.len() != 5 {
				return Err(std::io::Error::new(
					std::io::ErrorKind::InvalidData,
					"All tables must have 5 columns!",
				));
			}

			for (j, &val) in vals.iter().enumerate() {
				let num = if let Ok(val) = val.parse() {
					val
				} else {
					return Err(std::io::Error::new(
						std::io::ErrorKind::InvalidData,
						"Tables must contain numbers!",
					));
				};

				bingo_table[i][j] = num;
			}
		}

		Ok(BingoTable { table: bingo_table })
	}
}

fn part1<I>(tables: &[BingoTable], mut number_pool: I)
where
	I: Iterator<Item = u32>,
{
	let mut drawn_numbers = Vec::new();

	loop {
		// draw the next number
		drawn_numbers.push(number_pool.next().unwrap_or_else(|| {
			panic!("All available numbers are drawn and there is no winner yet!")
		}));

		// check if there is a winner
		//
		// NOTE: This stops after finding a winner. If there are multiple winners
		// we should probably still continue to check if they have a higher result.
		for table in tables {
			if table.completed(&drawn_numbers) {
				println!(
					"Score of the first completed board: {}",
					// save to unwrap since there are at least 5 elements drawn
					table.score(&drawn_numbers) * drawn_numbers.last().unwrap()
				);

				return;
			}
		}
	}
}

fn part2<I>(tables: &[BingoTable], mut number_pool: I)
where
	I: Iterator<Item = u32>,
{
	let mut drawn_numbers = Vec::new();

	let mut completed = Vec::new();

	loop {
		// draw the next number
		drawn_numbers.push(number_pool.next().unwrap_or_else(|| {
			panic!("All available numbers are drawn and there is no winner yet!")
		}));

		// Really dirty and inefficient. Since we have so few tables, this doesn't really
		// matter though. This assumes that there is exactly ONE board that is completed
		// last and ALL boards get completed.
		let incomplete: Vec<_> = tables
			.iter()
			.enumerate()
			.filter(|(i, _)| !completed.contains(i))
			.collect();

		for (i, table) in &incomplete {
			if table.completed(&drawn_numbers) {
				// This is the last completed board.
				if incomplete.len() == 1 {
					println!(
						"Score of the last completed board: {}",
						// save to unwrap since there are at least 5 elements drawn
						table.score(&drawn_numbers) * drawn_numbers.last().unwrap()
					);
					return;
				}

				completed.push(*i);
			}
		}
	}
}

fn main() -> std::io::Result<()> {
	let mut input_file = File::open("input")?;
	let mut input = String::new();
	input_file.read_to_string(&mut input)?;

	let mut segments = input.split("\n\n");

	let number_pool = segments
		.next()
		.ok_or_else(|| {
			std::io::Error::new(
				std::io::ErrorKind::InvalidData,
				"Expected at least 2 lines",
			)
		})?
		.split(',')
		.map(|s| s.parse::<u32>().unwrap());

	let mut tables = Vec::new();
	for seg in segments {
		let table = seg.parse::<BingoTable>()?;

		tables.push(table);
	}

	part1(&tables, number_pool.clone());
	part2(&tables, number_pool);

	Ok(())
}
