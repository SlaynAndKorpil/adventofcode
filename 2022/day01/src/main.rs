fn main() -> std::io::Result<()> {
	let input = std::fs::read_to_string("input")?;
	let input = input
		.split("\n\n")
		.map(|lines| {
			lines
				.split('\n')
				.map(|line| line.parse::<i32>().unwrap_or_default()) // 0 when the line is empty
				.sum::<i32>()
		})
		.collect::<Vec<_>>();

	part1(&input);
	part2(&input);

	Ok(())
}

fn part1(input: &[i32]) {
	if let Some(max_calories) = input.iter().max() {
		println!("Max calories: {max_calories}");
	} else {
		println!("No food :(");
	}
}

fn part2(input: &[i32]) {
	let mut max = [0i32; 3];

	for &i in input {
		// Just ignore this part.
		if i > max[0] {
			if i > max[1] {
				if i > max[2] {
					max.copy_within(1..2, 0);
					max[2] = i;
				} else {
					max[0] = max[1];
					max[1] = i;
				}
			} else {
				max[0] = i;
			}
		}
	}

	println!("Total Calories: {}", max.iter().sum::<i32>());
}
