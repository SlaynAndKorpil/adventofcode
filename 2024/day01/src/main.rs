use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
	part_1()?;
	part_2()?;

	Ok(())
}

fn part_1() -> Result<(), Box<dyn Error>> {
	let (mut left, mut right) = get_lists("input.txt")?;

	left.sort();
	right.sort();

	let difference: u32 = left
		.iter()
		.zip(right)
		.map(|(left, right)| left.abs_diff(right))
		.sum();

	println!("{difference}");

	Ok(())
}

fn part_2() -> Result<(), Box<dyn Error>> {
	let (left, right) = get_lists("input.txt")?;

	let mut right_frequency = std::collections::BTreeMap::new();

	for number in &right {
		*right_frequency.entry(number).or_insert(0) += 1;
	}

	let similarity: u32 = left
		.iter()
		.map(|num| num * right_frequency.get(num).unwrap_or(&0))
		.sum();

	println!("{similarity}");

	Ok(())
}

fn get_lists(file_name: &str) -> Result<(Vec<u32>, Vec<u32>), Box<dyn Error>> {
	let input = std::fs::read_to_string(file_name)?;

	let mut left = vec![];
	let mut right = vec![];

	for line in input.lines() {
		let mut words = line.split("   ");

		let first = words
			.next()
			.expect("Expect each line to have two numbers, found none.");
		let second = words
			.next()
			.expect("Expect each line to have two numbers, found only one.");

		if words.next().is_some() {
			panic!("Got too many words in a line.");
		}

		left.push(first.parse::<u32>()?);
		right.push(second.parse::<u32>()?);
	}

	Ok((left, right))
}
