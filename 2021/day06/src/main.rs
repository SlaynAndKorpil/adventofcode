fn simulate_days(days: usize, fish_times: &mut [u64; 9]) {
	for _ in 0..days {
		let new_fish = fish_times[0];

		// shift all entries to the left
		for i in 0..8 {
			fish_times[i] = fish_times[i + 1];
		}

		// add all fish that were at 0 to 6 and put their children at 8
		fish_times[6] += new_fish;
		fish_times[8] = new_fish;
	}
}

fn part1(init_fish_times: &[u64; 9]) -> u64 {
	let mut copy = *init_fish_times;

	simulate_days(80, &mut copy);

	copy.into_iter().sum()
}

fn part2(init_fish_times: &[u64; 9]) -> u64 {
	let mut copy = *init_fish_times;

	simulate_days(256, &mut copy);

	copy.into_iter().sum()
}

/// See [Desu solution](https://gist.github.com/m1el/f9d0eff1be2cbd740e7a77448ef7667c) for
/// the ULTRA-PERF version using matrices calculated at compile time for the specific day
/// counts.
fn main() -> std::io::Result<()> {
	let input = std::fs::read_to_string("input")?;

	let mut fish_times = [0u64; 9];

    let times = input.trim().split(',').map(|x| {
		x.parse::<usize>()
			.unwrap_or_else(|e| panic!("Unable to parse \"{}\": {}", x, e))
	});
	for time in times {
		fish_times[time] += 1;
	}

	let part1_sol = part1(&fish_times);
	let part2_sol = part2(&fish_times);

	println!("Part 1: {}", part1_sol);
	println!("Part 2: {}", part2_sol);

    const SIM_COUNT: usize = 10_000_000;
    let inst = std::time::Instant::now();
    for _ in 0..SIM_COUNT {
        let _ = part2(&fish_times);
    }
    println!("in {:10.3}ns per 256 day simulation", inst.elapsed().as_nanos() as f64 / SIM_COUNT as f64);

	Ok(())
}
