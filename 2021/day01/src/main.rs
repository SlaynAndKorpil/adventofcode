use std::io::Read;

fn main() -> std::io::Result<()> {
	let mut file = std::fs::File::open("input")?;
	let mut inputs = String::new();
	file.read_to_string(&mut inputs)?;

	// We know the file only contains integer numbers
	let inputs: Vec<u32> = inputs.lines().map(|x| x.parse::<u32>().unwrap()).collect();

	println!("{}", increased1(Vec::clone(&inputs)));
	println!("{}", increased2(inputs));

	Ok(())
}

fn increased1(inputs: Vec<u32>) -> u32 {
	let mut inputs = inputs.iter();

	let mut last: u32 = if let Some(&val) = inputs.next() {
		val
	} else {
		return 0;
	};

	let mut increased = 0;

	for &line in inputs {
		let val: u32 = line;

		if val > last {
			increased += 1;
		}

		last = val;
	}

	increased
}

fn increased2(inputs: Vec<u32>) -> u32 {
	let mut increased = 0;

	if inputs.len() >= 3 {
		let mut last = inputs[0] + inputs[1] + inputs[2];

		for idx in 1..inputs.len() - 2 {
			let sum = inputs[idx] + inputs[idx + 1] + inputs[idx + 2];

			if sum > last {
				increased += 1;
			}

			last = sum;
		}
	}

    increased
}
